@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                <div class="panel-heading">Map</div>

                <div class="panel-body" id="map" style="height: 400px;"></div>
            </div>
            <div class="panel panel-default">
                <input type="text" id="search" class="form-control" placeholder="Search">
                <div class="panel-heading">List</div>

                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Lat</th>
                                <th>Lng</th>
                                <th>Marker name</th>
                                <th>Action</th>
                                <th>Visited</th>
                            </tr>
                        </thead>
                        <tbody id="list"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('external_js')
    <script type="text/javascript" src="{{ URL::asset('assets/js/map.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCx3Q4gPyYLwYvQWx1DqMduJKVXzuDqHhs&callback=initMap"
            async defer></script>
    <script type="text/javascript">
        function initMap(){
            // create Map object
            var map = new Maps();
        }
    </script>
@endsection

