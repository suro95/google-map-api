<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'locations';
    /**
     * insert new location in db
     *
     * @return int
     */
    public function addLocation(array $data)
    {
        return DB::table($this->table)->insert($data);
    }
    /**
     * select all locations
     *
     * @return object
     */
    public function selectAll()
    {
        return DB::table($this->table)->select('*')->get();
    }
    /**
     * select location
     *
     * @return object
     */
    public function select(array $data, $select = '*')
    {
        return DB::table($this->table)->select($select)->where($data)->get();
    }

    /**
     * delete location
     *
     * @return object
     */
    public function deleteLocation(array $data)
    {
        return DB::table($this->table)->where($data)->delete();
    }

    /**
     * update location
     *
     * @return
     */
    public function updateLocation(array $where, array $data)
    {
        return DB::table($this->table)->where($where)->update($data);
    }
}
