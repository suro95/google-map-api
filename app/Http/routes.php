<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/dashboard', 'DashboardController@index');
Route::post('/add-location', 'MapController@addLocation');
Route::post('/get-locations', 'MapController@getAllLocations');
Route::post('/delete-location', 'MapController@deleteLocation');
Route::post('/update-location', 'MapController@updateLocation');