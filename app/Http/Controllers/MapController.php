<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

use App\Models\Locations;
use App\Models\User;

class MapController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * add locationl in db.
     *
     * @return json
     */
    public function addLocation(Request $request)
    {
        $location = new Locations();
        $location->addLocation([
            'lat'           => $request->input('lat'),
            'lng'           => $request->input('lng'),
            'name'          => $request->input('marker_name'),
            'user_id'       => Auth::user()->id
        ]);

        echo json_encode(['status' => 'success' ]);die;
    }

    /**
     * delete locationl in db.
     *
     * @return json
     */
    public function deleteLocation(Request $request)
    {
        $location = new Locations();
        $location->deleteLocation([
            'id'          => $request->input('id'),
            'user_id'       => Auth::user()->id
        ]);

        echo json_encode(['status' => 'success']);die;
    }

    /**
     * Get all locations
     *
     * @return json
     */
    public function getAllLocations()
    {
        $location = new Locations();
        $locations = $location->select(['user_id' => Auth::user()->id], ['lat', 'lng', 'name', 'id', 'visited']);

        echo json_encode(['locations' => $locations]);die;
    }

    /**
     * update locations
     *
     * @return json
     */
    public function updateLocation(Request $request)
    {
        $location = new Locations();
        $location->updateLocation([
            'id'            => $request->input('id'),
            'user_id'       => Auth::user()->id
        ],[
            'visited'       => $request->input('visited'),
        ]);

        echo json_encode(['status' => 'success']);die;
    }
}
