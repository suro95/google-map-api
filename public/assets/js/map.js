// constructor function
var Maps = function(icon_list){
    this.map = this.initMap();
    this.updateAllMarkers();
    this.bindEvent();
    this.allMarkers = [];
}

// Map functions start
Maps.prototype.initMap = function(){
    var _this = this;

    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 8
    });

    google.maps.event.addListener(map, 'click', function(event) {
        _this.addMarker(event.latLng);
    });

    return map;
}

// event on clicking on map
Maps.prototype.addMarker = function(location) {
    var _this = this,
        location_obj = {lat: location.lat(), lng: location.lng()},
        marker = _this.showMarker(location_obj);

    var marker_name = prompt('Put marker name.');

    if(marker_name){
        marker.setLabel(marker_name);
        _this.sendMarker(marker_name, location);
        _this.allMarkers.push(marker);
    }
}


// add marker icon in map
Maps.prototype.showMarker = function(location, lable) {
    var _this = this;

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng( location.lat,location.lng),
        map: _this.map,
    });
    marker.setLabel(lable);

    return marker;
}

// add marker in map and add in db
Maps.prototype.sendMarker = function(marker_name, location){
    Promise.resolve({
        data : {
            url : base_url + "/add-location",
            data : {
                marker_name: marker_name,
                lat: location.lat(),
                lng: location.lng(),
                _token: $('#_token').val()
            },
            type : 'POST'
        },
        this : this
    })
    .then(this.PromiseAjax)
    .then(this.updateAllMarkers)
    .catch(function(err){
        console.log(err);
    });
}


// get all location from db and show location list/ add marker
Maps.prototype.updateAllMarkers = function(obj){
    if(obj)
        var _this = obj.this;
    else
        var _this = this;

    Promise.resolve({
        data : {
            url : base_url + "/get-locations",
            data : {
                _token: $('#_token').val()
            },
            type : 'POST'
        },
        this : _this
    })
    .then(_this.PromiseAjax)
    .then(_this.appendLocations)
    .then(_this.removeAllMarkers)
    .then(_this.showAllMarkers)
    .catch(function(err){
        console.log(err);
    });
}


// show all location in list
Maps.prototype.appendLocations = function(obj){
    return new Promise(function(resolve,reject){
        var data = obj.data,
            _this = obj.this,
            locations = data.locations;

        $('#list').html('');
        $('#search').val('');

        for(var i in locations){
            var htmlData = {
                lat : locations[i].lat,
                lng : locations[i].lng,
                name : locations[i].name,
                id : locations[i].id,
                visited : locations[i].visited,
            }

            _this.appendHTML(htmlData);
        }

        return resolve(obj);
    });
}


// add all markers in map
Maps.prototype.showAllMarkers = function(obj){
    return new Promise(function(resolve,reject){
        var data = obj.data,
            _this = obj.this,
            locations = data.locations,
            location_obj, marker;

        for(var i in locations){
            location_obj = {lat: locations[i].lat, lng: locations[i].lng};
            marker = _this.showMarker(location_obj, locations[i].name);
            _this.allMarkers.push(marker);
        }

        return resolve(obj);
    });
}


// remove all markers from map
Maps.prototype.removeAllMarkers = function(obj){
    return new Promise(function(resolve,reject){
        var _this = obj.this,
            markers = _this.allMarkers;

        for(var i in markers){
            markers[i].setMap(null);
        }
        _this.allMarkers = [];
        return resolve(obj);
    });
}


// remove location from list and map
Maps.prototype.removeLocation = function(id){
    Promise.resolve({
        data : {
            url : base_url + "/delete-location",
            data : {
                _token: $('#_token').val(),
                id: id
            },
            type : 'POST'
        },
        this : this
    })
    .then(this.PromiseAjax)
    .then(this.updateAllMarkers)
    .catch(function(err){
        console.log(err);
    });
}


// change visited value for location
Maps.prototype.changeVisited = function($this){
    if($this.is(':checked'))
        var visited = 'yes';
    else
        var visited = 'no';

    Promise.resolve({
        data : {
            url : base_url + "/update-location",
            data : {
                _token: $('#_token').val(),
                id: $this.data('id'),
                visited: visited,
            },
            type : 'POST'
        },
        this : this
    })
    .then(this.PromiseAjax)
    .catch(function(err){
        console.log(err);
    });
}

// search in locations
Maps.prototype.search = function(text) {
    $.each($('.location_name'), function(k, v){
        if($(v).text().indexOf(text) !== -1){
            $(v).parent().show();
        }else{
            $(v).parent().hide();
        }
    })
}
// Map functions end


// helper functions start
// send ajax request
Maps.prototype.PromiseAjax = function(obj){
    return new Promise(function(resolve,reject){
        $.ajax({
            url: obj.data.url,
            data: obj.data.data,
            type: obj.data.type,
            dataType: 'json',
            success: function(data){
                obj.data = data
                return resolve(obj);
            },
            error : function(err){
                return reject(err);
            }
        });
    });
}

// add html for each location
Maps.prototype.appendHTML = function(data){
    $('#list').append('<tr>' +
            '<td>'+data.id+'</td>' +
            '<td>'+data.lat+'</td>' +
            '<td>'+data.lng+'</td>' +
            '<td class="location_name">'+data.name+'</td>' +
            '<td><button class="remove" data-id="'+data.id+'">Remove</button></td>' +
            '<td><input data-id="'+data.id+'" type="checkbox" class="visited" '+((data.visited == 'yes')? 'checked' : '' )+'></td></tr>');
}
// helper functions end

// initialization start, binde event
Maps.prototype.bindEvent = function(){
    var _this = this;

    $(document).on('click','.remove',function(e){
        _this.removeLocation($(this).data('id'));
    })

    $(document).on('click','.visited',function(e){
        _this.changeVisited($(this));
    })

    $(document).on('input','#search',function(e){
        _this.search($(this).val());
    })
}

